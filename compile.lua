
---@alias TokenType "."|","|">"|"<"|":"|"-"|"+"|"("|")"|"|>"|"->"|"*"|"&"|"|"|"=="|"~="|"~"|"="|"str"|"id"

---@class Token
---@field t TokenType
---@field v string

---@return Token
local function T(t, v)
    return { t = t, v = v or t }
end

---@generic T
---@param t table<any, T>
---@param v T
local function contains(t, v)
    for _, x in ipairs(t) do
        if x == v then return true end
    end

    return false
end

---@param f file*
local function lex(f)
    local c = f:read(1)
    return function()
        if c == nil then return nil end

        while string.match(c, '%s') do
            c = f:read(1)
            if c == nil then return nil end
        end

        local CHARS = {
            '.', ':', '(', ')', '[', ']',
            '{', '}', '+', '/', '*', '&',
        }

        if contains(CHARS, c) then
            local oc = c
            c = f:read(1)
            return T(oc)
        end

        if c == "|" or c == "-" then
            local oc = c
            c = f:read(1)
            if c == ">" then
                c = f:read(1)
                return T(oc..'>')
            else
                return T(oc)
            end
        end

        if c == ">" or c == "<" then
            local oc = c
            c = f:read(1)
            if c == "=" then
                c = f:read(1)
                return T(oc..'=')
            else
                return T(oc)
            end
        end

        if contains({'=', '~', '>', '<'}, c) then
            local oc = c
            c = f:read(1)
            if c == "=" then
                c = f:read(1)
                return T(oc..'=')
            else
                return T(oc)
            end
        end

        if string.match(c, '%a') then
            local s = ""
            while string.match(c, '%a') do
                s = s .. c
                c = f:read(1)
            end
            return T('id', s)
        end

        if c == '"' then
            c = f:read(1)
            local s = ""
            while c ~= '"' do
                s = s .. c
                c = f:read(1)
            end
            c = f:read(1)
            return T('str', s)
        end

        error("error: unknown character: '"..c.."'")
    end
end

---@param filename string
local function print_tokens(filename)
    local f, errmsg = io.open(filename)
    if f == nil then
        error(errmsg)
    end

    for t in lex(f) do
        if t.t == '|>' then io.write('\n') end
        if t.t == 'str' then io.write('"') end
        io.write(t.v)
        if t.t == 'str' then io.write('"') end
        io.write(' ')
    end

    io.write('\n')

    io.close(f)
end

print_tokens('example.waf')

---@class Parser
---@field toks Token[]
local Parser = {}

---@param toks Token[]
function Parser:new(toks)
    local o = {
        toks = toks,
    }

    setmetatable(o, self)
    self.__index = self

    return o
end

---@class Maybe<T>: { isOk: boolean, value?: T, fail?: string }

---@generic T
---@return Maybe<T>
function Ok(x) return { isOk = true, value = x } end

---@generic T
---@return Maybe<T>
function Fail(fail) return { isOk = true, fail = fail } end

---@alias ParserMaybe<T> Maybe<{ [1]: integer, [2]: T }>

---@generic T
---@param i integer
---@param x T
---@return ParserMaybe<T>
local function pOk(i, x) return { isOk = true, value = {i, x} } end

---@generic T
---@param i integer
---@param f fun(i: integer): ParserMaybe<T>
---@return integer, T|string
local function parser(i, f)
    local r = f(i)
    if r.isOk then
        return r.value[1], r.value[2]
    else
        return i, r.fail
    end
end

function Parser:parse_atom(i)
    return parser(i, function(i)
        local t = self.toks[i]
        i = i + 1
        if t.t == "str" then return pOk(i, { t = 'str', v = t.v }) end
        if t.t == "id" then return pOk(i, { t = 'id', v = t.v }) end
        if t.t == "(" then
            local e = self:parse_expression()
            t = self.toks[i]
            i = i + 1
            if t.t ~= ")" then
                return Fail("expected ')', got '"..t.t.."' instead!")
            end
            return pOk(i, e)
        end
        return Fail("expected atom, got '"..t.t.."' instead!")
    end)
end

function Parser:parse_suffix(i, e)
    return parser(i, function(i)
        if e == nil then
            e = Parser:parse_atom()
        end

        while contains({':', '.'}, self.toks[i]) do
            local op = self.toks[i]
            i = i + 1
            if self.toks[i].t ~= "id" then
                error("expected id, got '"..self.toks[i].t.."' instead!")
            end
            local id = self.toks[i].v
            i = i + 1
            e = { t = op, lhs = e, rhs = id }
        end 
    end)
end

function Parser:parse_mul()
    local lhs = self:parse_suffix()
    while contains({'/', '*'}, self:_peek().t) do
        local op = self:_get().t
        local rhs = self:parse_suffix()
        lhs = { t = op, lhs = lhs, rhs = rhs }
    end
    return lhs
end

function Parser:parse_sum()
    local lhs = self:parse_mul()
    while contains({'+', '-'}, self:_peek().t) do
        local op = self:_get().t
        local rhs = self:parse_mul()
        lhs = { t = op, lhs = lhs, rhs = rhs }
    end
    return lhs
end

function Parser:parse_comp()
    local lhs = self:parse_sum()
    while contains({'>', '<', '>=', '<='}, self:_peek().t) do
        local op = self:_get().t
        local rhs = self:parse_sum()
        lhs = { t = op, lhs = lhs, rhs = rhs }
    end
    return lhs
end

function Parser:parse_equal()
    local lhs = self:parse_comp()
    while contains({'==', '~='}, self:_peek().t) do
        local op = self:_get().t
        local rhs = self:parse_comp()
        lhs = { t = op, lhs = lhs, rhs = rhs }
    end
    return lhs
end

function Parser:parse_and_or()
    local lhs = self:parse_equal()
    while contains({'|', '&'}, self:_peek().t) do
        local op = self:_get().t
        local rhs = self:parse_equal()
        lhs = { t = op, lhs = lhs, rhs = rhs }
    end
    return lhs
end

function Parser:parse_expression()
    local e = self:parse_and_or()
    while self:_peek().t == '|>' do
        e = self:parse_partial_appl(e)
    end
end

function Parser:parse()
    return self:parse_expression()
end
